'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Button = require('./Button');

var _Button2 = _interopRequireDefault(_Button);

var _Text = require('./Text');

var _Text2 = _interopRequireDefault(_Text);

var _colors = require('../colors');

var _colors2 = _interopRequireDefault(_colors);

var _BlockBox = require('./BlockBox');

var _BlockBox2 = _interopRequireDefault(_BlockBox);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var base = function base() {
  return {};
};

var DEFAULT_TEXT_COLOR = _colors2.default.mGrey7;
var DEFAULT_BG_COLOR = _colors2.default.mGrey0;

var styleWrap = function styleWrap() {
  var textColor = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : DEFAULT_TEXT_COLOR;
  var bgColor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : DEFAULT_BG_COLOR;
  return {
    borderWidth: 3,
    borderStyle: 'dashed',
    borderColor: textColor,

    position: 'relative',
    background: bgColor
  };
};

var styleText = function styleText(color, size) {
  return {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    fontSize: size,
    color: color,
    paddingTop: 10,
    pointerEvents: 'none',
    display: 'inline-block',
    textAlign: 'center'

  };
};
var styleParagraph = function styleParagraph() {
  return {
    textAlign: 'center',
    alignSelf: 'center'
  };
};

var styleInput = function styleInput() {
  return {
    margin: 0,
    width: '100%',
    height: 100,
    opacity: 0,
    cursor: 'pointer'
  };
};

var FileUpload = function FileUpload(_ref) {
  var onChange = _ref.onChange,
      fontSize = _ref.fontSize,
      color = _ref.color,
      background = _ref.background,
      rest = _objectWithoutProperties(_ref, ['onChange', 'fontSize', 'color', 'background']);

  return _react2.default.createElement(
    _BlockBox2.default,
    { style: styleWrap(color, background) },
    _react2.default.createElement('input', { style: styleInput(), type: 'file', onChange: onChange }),
    _react2.default.createElement(
      _BlockBox2.default,
      { style: styleText(color, fontSize) },
      _react2.default.createElement(
        _Text2.default,
        { center: true },
        'P\u0159et\xE1hni sem obr\xE1zek'
      ),
      _react2.default.createElement(
        _Text2.default,
        { center: true },
        'nebo'
      ),
      _react2.default.createElement(
        _Button2.default,
        { color: 'red', margin: 0, size: 10 },
        'Vyber soubor'
      )
    )
  );
};

exports.default = FileUpload;