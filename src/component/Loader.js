import * as React from 'react';
import Image from './Image';

import loader from './../../resource/image/loader.svg';

const Loader = () =>
    <Image source={loader}/>;

export default Loader;
