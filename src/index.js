import Box from './component/Box';
import BlockBox from './component/BlockBox';
import FillBox from './component/FillBox';

import Button from './component/Button';
import FileInput from './component/FileUpload';

import Image from './component/Image';
import ImageBox from './component/ImageBox';

import Text from './component/Text';
import Heading from './component/Heading';

import colors from './colors';

export {
  Box,
  BlockBox,
  FillBox,
  FileInput,
  Image,
  ImageBox,
  Text,
  Heading,

  colors,
};
