import * as React from 'react';

import Text from './Text';


const Heading = ({children, style, ...rest}) =>
    <Text style={{...style}}{...rest}>{children}</Text>;

export default Heading;
