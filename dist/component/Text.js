'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _Box = require('./Box');

var _Box2 = _interopRequireDefault(_Box);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

//import type {BoxProps} from './Box';


////import {withTheme} from './fela.js';
//import theme from './../theme';

//const REACT_ENV = process.env.REACT_ENV;

//export type TextProps = BoxProps & {
//  align?: 'left' | 'right' | 'center' | 'justify',
//  bold?: boolean,
//  color?: string,
//  decoration?: 'none' | 'underline' | 'line-through',
//  fontFamily?: string,
//  italic?: boolean,
//  lineHeight?: number,
//  size?: number
//  // TODO: shadowColor, shadowOffset, shadowRadius.
//};

//console.log(theme);

var Text = function Text(props) {
  //console.log(theme);

  var align = props.align,
      textAlign = props.textAlign,
      bold = props.bold,
      color = props.color,
      decoration = props.decoration,
      fontFamily = props.fontFamily,
      italic = props.italic,
      lineHeight = props.lineHeight,
      fontSize = props.fontSize,
      fontStyle = props.fontStyle,
      fontWeight = props.fontWeight,
      restProps = _objectWithoutProperties(props, ['align', 'textAlign', 'bold', 'color', 'decoration', 'fontFamily', 'italic', 'lineHeight', 'fontSize', 'fontStyle', 'fontWeight']);

  var propsStyle = restProps.style,
      restWithoutStyle = _objectWithoutProperties(restProps, ['style']);

  var style = _extends({
    // color: theme.colors[color],
    color: color,
    fontFamily: fontFamily,
    fontStyle: fontStyle,
    fontSize: fontSize,
    fontWeight: fontWeight,
    lineHeight: lineHeight,
    textAlign: textAlign
  }, align ? { textAlign: align } : null, bold ? { fontWeight: 300 } : null, decoration ? { textDecoration: decoration } : null, italic ? { fontStyle: 'italic' } : null, lineHeight ? { lineHeight: lineHeight } : null, propsStyle);

  return _react2.default.createElement(_Box2.default, _extends({ style: style }, restWithoutStyle));
};

//withTheme(Text);

exports.default = Text;