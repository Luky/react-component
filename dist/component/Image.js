'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Box = require('./Box');

var _Box2 = _interopRequireDefault(_Box);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var base = function base() {
  return {};
};

var Image = function Image(_ref) {
  var src = _ref.src,
      source = _ref.source,
      rest = _objectWithoutProperties(_ref, ['src', 'source']);

  if (typeof src !== 'undefined') {
    throw new Error('[own] Use src on Image object is forbidden, use source attribute instead.');
  }

  return _react2.default.createElement(_Box2.default, _extends({ as: 'img',
    style: _extends({}, base(rest)),
    src: source
  }, rest));
};

exports.default = Image;