import * as React from 'react';

import Box from './Box';

const FillBox = ({children, ...rest}) =>
    <Box fill {...rest}>{children}</Box>;

export default FillBox;
