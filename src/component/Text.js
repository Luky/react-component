// @flow

//import type {BoxProps} from './Box';
import Box from './Box';
import React from 'react';
////import {withTheme} from './fela.js';
//import theme from './../theme';

//const REACT_ENV = process.env.REACT_ENV;

//export type TextProps = BoxProps & {
//  align?: 'left' | 'right' | 'center' | 'justify',
//  bold?: boolean,
//  color?: string,
//  decoration?: 'none' | 'underline' | 'line-through',
//  fontFamily?: string,
//  italic?: boolean,
//  lineHeight?: number,
//  size?: number
//  // TODO: shadowColor, shadowOffset, shadowRadius.
//};

//console.log(theme);

const Text = (props) => {
  //console.log(theme);

  const {
    align,
    textAlign,
    bold,
    color,
    decoration,
    fontFamily,
    italic,
    lineHeight,
    fontSize,
    fontStyle,
    fontWeight,
    ...restProps
  } = props;

  const { style: propsStyle, ...restWithoutStyle } = restProps;
  const style = {
    // color: theme.colors[color],
    color,
    fontFamily,
    fontStyle,
    fontSize,
    fontWeight,
    lineHeight,
    textAlign,
    ...(align ? { textAlign: align } : null),
    ...(bold ? { fontWeight: 300 } : null),
    ...(decoration ? { textDecoration: decoration } : null),
    ...(italic ? { fontStyle: 'italic' } : null),
    ...(lineHeight ? { lineHeight } : null),
    ...propsStyle
  };

  return <Box style={style} {...restWithoutStyle} />;
};

//withTheme(Text);

export default Text;
