'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var React = _interopRequireWildcard(_react);

var _Box = require('./Box');

var _Box2 = _interopRequireDefault(_Box);

var _Image = require('./Image');

var _Image2 = _interopRequireDefault(_Image);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var ImageBox = function ImageBox(_ref) {
  var source = _ref.source;

  return React.createElement(
    _Box2.default,
    { style: {
        display: 'flex',
        justifyContent: 'center'
      } },
    React.createElement(
      _Box2.default,
      { style: { display: 'block' } },
      React.createElement(_Image2.default, { style: {
          maxWidth: '100%',
          maxHeight: 'calc(100vh - 200px)'
        }, source: source })
    )
  );
};

exports.default = ImageBox;