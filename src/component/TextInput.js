import * as React from 'react';

const base = () => ({
  display: 'block',
  width: '100%',
  background: 'transparent',
  fontSize: 15,
  color: '#43383e',
  lineHeight: 1.2,
  padding: '0 5px',
  height: 62,
});

const TextInput = ({
                     value,
                     placeholder,
                     borderColor,
                     type = 'text',
                     style,
                     ...rest
                   }) =>
    <input
        type={type}
        style={{
          ...base(...rest),
          ...style,
        }}
    />;

export default TextInput;
