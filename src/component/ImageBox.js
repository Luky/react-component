// @flow

import * as React from 'react';

import Box from './Box';
import Image from './Image';

const ImageBox = ({source}) => {
  return (
      <Box style={{
        display: 'flex',
        justifyContent: 'center',
      }}>
        <Box style={{display: 'block'}}>
          <Image style={{
            maxWidth: '100%',
            maxHeight: 'calc(100vh - 200px)',
          }} source={source}/>
        </Box>
      </Box>
  );
};

export default ImageBox;

