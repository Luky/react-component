import * as React from 'react';

import Box from './Box';

const BlockBox = ({children, ...rest}) =>
    <Box block {...rest}>{children}</Box>;

export default BlockBox;
