// @flow
import React from 'react';
import Button from './Button';
import Text from './Text';
import Color from '../colors';
import BlockBox from './BlockBox';

const base = () => ({});

const DEFAULT_TEXT_COLOR = Color.mGrey7;
const DEFAULT_BG_COLOR   = Color.mGrey0;

const styleWrap = (
    textColor = DEFAULT_TEXT_COLOR,
    bgColor   = DEFAULT_BG_COLOR) => ({
  borderWidth: 3,
  borderStyle: 'dashed',
  borderColor: textColor,

  position: 'relative',
  background: bgColor,
});

const styleText      = (color, size) => ({
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  fontSize: size,
  color: color,
  paddingTop: 10,
  pointerEvents: 'none',
  display: 'inline-block',
  textAlign: 'center',

});
const styleParagraph = () => ({
  textAlign: 'center',
  alignSelf: 'center',
});

const styleInput = () => ({
  margin: 0,
  width: '100%',
  height: 100,
  opacity: 0,
  cursor: 'pointer',
});

const FileUpload = ({onChange, fontSize, color, background, ...rest}: any) => {

  return (
      <BlockBox style={styleWrap(color, background)}>
        <input style={styleInput()} type="file" onChange={onChange}/>
        <BlockBox style={styleText(color, fontSize)}>
          <Text center>Přetáhni sem obrázek</Text>
          <Text center>nebo</Text>
          <Button color={'red'} margin={0} size={10}>Vyber soubor</Button>
        </BlockBox>
      </BlockBox>
  );
};

export default FileUpload;
