// @flow
import React from 'react';

import Box from './Box';

const base = () => ({});

const Image = ({src, source, ...rest}) => {
  if (typeof src !== 'undefined') {
    throw new Error(
        '[own] Use src on Image object is forbidden, use source attribute instead.');
  }

  return (
      <Box as='img'
           style={{...base(rest)}}
           src={source}
           {...rest}
      ></Box>
  );
};

export default Image;

