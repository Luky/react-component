'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var React = _interopRequireWildcard(_react);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var base = function base() {
  return {
    display: 'block',
    width: '100%',
    background: 'transparent',
    fontSize: 15,
    color: '#43383e',
    lineHeight: 1.2,
    padding: '0 5px',
    height: 62
  };
};

var TextInput = function TextInput(_ref) {
  var value = _ref.value,
      placeholder = _ref.placeholder,
      borderColor = _ref.borderColor,
      _ref$type = _ref.type,
      type = _ref$type === undefined ? 'text' : _ref$type,
      style = _ref.style,
      rest = _objectWithoutProperties(_ref, ['value', 'placeholder', 'borderColor', 'type', 'style']);

  return React.createElement('input', {
    type: type,
    style: _extends({}, base.apply(undefined, _toConsumableArray(rest)), style)
  });
};

exports.default = TextInput;