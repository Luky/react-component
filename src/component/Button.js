// @flow
import React from 'react';

import Text from './Text';

import Colors from './../colors';

const buttonBorderSize = '1px';
const buttonFontSize   = '0.95em';
const buttonPadding    = '4px 12px';
const buttonFontWeight = '400';
const buttonFontFamily = 'Muli';
//const buttonColorModifier = -18;

const base = () => ({
  margin: '5px',
  display: 'inline-block',
  fontSize: buttonFontSize,
  fontWeight: buttonFontWeight,
  padding: buttonPadding,
  textShadow: 'none',
  transition: 'all 0.4s ease',
  cursor: 'pointer',
});

const colorized = props => {

  const colorText = (attr => {
    if (attr.color === 'facebook') return Colors.colorFacebookText;
    if (attr.color === 'google') return Colors.colorGooglePlusText;
    return Colors.mGrey0;
  })(props);

  const colorBackground = (attr => {
    if (attr.color === 'red') return Colors.mRed4;
    if (attr.color === 'orange') return Colors.mOrange4;
    if (attr.color === 'amber') return Colors.mAmber4;
    if (attr.color === 'yellow') return Colors.mYellow4;
    if (attr.color === 'green') return Colors.mGreen4;
    if (attr.color === 'lime') return Colors.mLime4;
    if (attr.color === 'cyan') return Colors.mCyan4;
    if (attr.color === 'lightblue') return Colors.mLightBlue4;
    if (attr.color === 'blue') return Colors.mBlue4;
    if (attr.color === 'teal') return Colors.mTeal4;
    if (attr.color === 'purple') return Colors.mPurple4;
    if (attr.color === 'brown') return Colors.mBrown4;
    if (attr.color === 'grey') return Colors.mBlueGrey4;
    if (attr.color === 'black') return Colors.mGrey8;
    if (attr.color === 'facebook') return Colors.colorFacebook;
    if (attr.color === 'google') return Colors.colorGooglePlus;
    if (attr.color === 'female') return Colors.mPink5;
    if (attr.color === 'male') return Colors.mIndigo6;
    if (attr.color === 'Cosplay') return Colors.mDeepPurple7;
    if (attr.color === 'Photo') return Colors.mAmber7;

    return Colors.mIndigo4;
  })(props);

  const colorBackgroundHover = (attr => {
    if (attr.color === 'facebook') return Colors.colorFacebookHover;
    if (attr.color === 'google') return Colors.colorGooglePlusHover;

    return Colors.mIndigo4;
  })(props);

  const invert = {
    background: colorText,
    color: colorBackground,
    borderColor: colorBackground,
  };

  return {
    fontFamily: buttonFontFamily,
    color: colorText,
    background: colorBackground,
    border: buttonBorderSize + ' transparent solid',

    '&:hover, &:active, &:focus': {
      color: colorBackgroundHover - 15,
      borderColor: colorBackgroundHover - 15,
      background: colorBackgroundHover,
    },
    ...(props.invert ? invert : {}),
  };

};

const Button = (props: any) => {

  return (
      <Text
          alignSelf="flex-start"
          style={{
            ...base(props),
            ...colorized(props),
          }}
          {...props}
      />
  );

};

export default Button;


