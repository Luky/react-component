'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ramda = require('ramda');

var R = _interopRequireWildcard(_ramda);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//import {withTheme} from './fela.js';

//export type BoxProps = {
//  as?: string | ((props: Object) => React.Element<*>),
//  isReactNative?: boolean,
//  style?: Object,
//
//  margin?: MaybeRhythm,
//  marginHorizontal?: MaybeRhythm,
//  marginVertical?: MaybeRhythm,
//  marginBottom?: MaybeRhythm,
//  marginLeft?: MaybeRhythm,
//  marginRight?: MaybeRhythm,
//  marginTop?: MaybeRhythm,
//
//  padding?: MaybeRhythm,
//  paddingHorizontal?: MaybeRhythm,
//  paddingVertical?: MaybeRhythm,
//  paddingBottom?: MaybeRhythm,
//  paddingLeft?: MaybeRhythm,
//  paddingRight?: MaybeRhythm,
//  paddingTop?: MaybeRhythm,
//
//  bottom?: MaybeRhythm,
//  height?: MaybeRhythm,
//  left?: MaybeRhythm,
//  maxHeight?: MaybeRhythm,
//  maxWidth?: MaybeRhythm,
//  minHeight?: MaybeRhythm,
//  minWidth?: MaybeRhythm,
//  right?: MaybeRhythm,
//  top?: MaybeRhythm,
//  width?: MaybeRhythm,
//
//  // Flexbox. Only what's compatible with React Native.
//  // github.com/facebook/react-native/blob/master/Libraries/StyleSheet/LayoutPropTypes.js
//  alignItems?: 'flex-start' | 'flex-end' | 'center' | 'stretch' | 'baseline',
//  alignSelf?: | 'auto'
//      | 'flex-start'
//      | 'flex-end'
//      | 'center'
//      | 'stretch'
//      | 'baseline',
//  flex?: number,
//  flexBasis?: number | string,
//  flexDirection?: 'row' | 'row-reverse' | 'column' | 'column-reverse',
//  flexGrow?: number,
//  flexShrink?: number,
//  flexWrap?: 'wrap' | 'nowrap',
//  justifyContent?: | 'flex-start'
//      | 'flex-end'
//      | 'center'
//      | 'space-between'
//      | 'space-around',
//
//  backgroundColor?: Color,
//  opacity?: number,
//  overflow?: 'visible' | 'hidden' | 'scroll',
//  position?: 'absolute' | 'relative',
//  zIndex?: number,
//
//  border?: string,
//  borderStyle?: 'solid' | 'dotted' | 'dashed',
//  borderWidth?: number,
//  borderBottomWidth?: number,
//  borderLeftWidth?: number,
//  borderRightWidth?: number,
//  borderTopWidth?: number,
//
//  borderRadius?: number,
//  borderBottomLeftRadius?: number,
//  borderBottomRightRadius?: number,
//  borderTopLeftRadius?: number,
//  borderTopRightRadius?: number,
//
//  borderColor?: Color,
//  borderBottomColor?: Color,
//  borderLeftColor?: Color,
//  borderRightColor?: Color,
//  borderTopColor?: Color
//};

//type BoxContext = ThemeContext & { renderer: { renderRule: () => string } };

// Emulate React Native to ensure the same styles for all platforms.
// https://facebook.github.io/yoga
// https://github.com/Microsoft/reactxp
// https://github.com/necolas/react-native-web
var reactNativeEmulationForBrowsers = {
  display: 'flex',
  // flexDirection: 'column',
  position: 'relative'
};

var reduce = function reduce(props, getValue) {
  return Object.keys(props).reduce(function (style, prop) {
    var value = props[prop];
    if (value === undefined) return style;
    return _extends({}, style, _defineProperty({}, prop, getValue(value)));
  }, {});
};

var justValue = function justValue(props) {
  return reduce(props, function (value) {
    return value;
  });
};

// https://facebook.github.io/react-native/releases/0.44/docs/layout-props.html#flex
// https://github.com/necolas/react-native-web expandStyle-test.js
var restrictedFlex = function restrictedFlex(flex) {
  var flexBasis = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'auto';
  var flexShrink = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
  var isReactNative = arguments[3];

  if (flex === undefined) return null;
  if (flex < 1) throw new Error('Not implemented yet');
  return isReactNative ? { flex: flex } : { flexBasis: flexBasis, flexGrow: flex, flexShrink: flexShrink };
};

// Color any type, because Flow can't infere props for some reason.
//const themeColor = (colors: any, props) =>
//    reduce(props, value => colors[value]);

var Box = function Box(props, _ref) {
  var renderer = _ref.renderer;

  var as = props.as,
      isReactNative = props.isReactNative,
      style = props.style,
      margin = props.margin,
      _props$marginHorizont = props.marginHorizontal,
      marginHorizontal = _props$marginHorizont === undefined ? margin : _props$marginHorizont,
      _props$marginVertical = props.marginVertical,
      marginVertical = _props$marginVertical === undefined ? margin : _props$marginVertical,
      _props$marginBottom = props.marginBottom,
      marginBottom = _props$marginBottom === undefined ? marginVertical : _props$marginBottom,
      _props$marginLeft = props.marginLeft,
      marginLeft = _props$marginLeft === undefined ? marginHorizontal : _props$marginLeft,
      _props$marginRight = props.marginRight,
      marginRight = _props$marginRight === undefined ? marginHorizontal : _props$marginRight,
      _props$marginTop = props.marginTop,
      marginTop = _props$marginTop === undefined ? marginVertical : _props$marginTop,
      padding = props.padding,
      _props$paddingHorizon = props.paddingHorizontal,
      paddingHorizontal = _props$paddingHorizon === undefined ? padding : _props$paddingHorizon,
      _props$paddingVertica = props.paddingVertical,
      paddingVertical = _props$paddingVertica === undefined ? padding : _props$paddingVertica,
      _props$paddingBottom = props.paddingBottom,
      paddingBottom = _props$paddingBottom === undefined ? paddingVertical : _props$paddingBottom,
      _props$paddingLeft = props.paddingLeft,
      paddingLeft = _props$paddingLeft === undefined ? paddingHorizontal : _props$paddingLeft,
      _props$paddingRight = props.paddingRight,
      paddingRight = _props$paddingRight === undefined ? paddingHorizontal : _props$paddingRight,
      _props$paddingTop = props.paddingTop,
      paddingTop = _props$paddingTop === undefined ? paddingVertical : _props$paddingTop,
      bottom = props.bottom,
      height = props.height,
      left = props.left,
      maxHeight = props.maxHeight,
      maxWidth = props.maxWidth,
      minHeight = props.minHeight,
      minWidth = props.minWidth,
      right = props.right,
      top = props.top,
      width = props.width,
      alignItems = props.alignItems,
      alignSelf = props.alignSelf,
      flex = props.flex,
      flexBasis = props.flexBasis,
      flexDirection = props.flexDirection,
      flexGrow = props.flexGrow,
      flexShrink = props.flexShrink,
      flexWrap = props.flexWrap,
      justifyContent = props.justifyContent,
      backgroundColor = props.backgroundColor,
      opacity = props.opacity,
      overflow = props.overflow,
      position = props.position,
      zIndex = props.zIndex,
      border = props.border,
      borderStyle = props.borderStyle,
      borderWidth = props.borderWidth,
      _props$borderBottomWi = props.borderBottomWidth,
      borderBottomWidth = _props$borderBottomWi === undefined ? borderWidth : _props$borderBottomWi,
      _props$borderLeftWidt = props.borderLeftWidth,
      borderLeftWidth = _props$borderLeftWidt === undefined ? borderWidth : _props$borderLeftWidt,
      _props$borderRightWid = props.borderRightWidth,
      borderRightWidth = _props$borderRightWid === undefined ? borderWidth : _props$borderRightWid,
      _props$borderTopWidth = props.borderTopWidth,
      borderTopWidth = _props$borderTopWidth === undefined ? borderWidth : _props$borderTopWidth,
      borderRadius = props.borderRadius,
      _props$borderBottomLe = props.borderBottomLeftRadius,
      borderBottomLeftRadius = _props$borderBottomLe === undefined ? borderRadius : _props$borderBottomLe,
      _props$borderBottomRi = props.borderBottomRightRadius,
      borderBottomRightRadius = _props$borderBottomRi === undefined ? borderRadius : _props$borderBottomRi,
      _props$borderTopLeftR = props.borderTopLeftRadius,
      borderTopLeftRadius = _props$borderTopLeftR === undefined ? borderRadius : _props$borderTopLeftR,
      _props$borderTopRight = props.borderTopRightRadius,
      borderTopRightRadius = _props$borderTopRight === undefined ? borderRadius : _props$borderTopRight,
      borderColor = props.borderColor,
      _props$borderBottomCo = props.borderBottomColor,
      borderBottomColor = _props$borderBottomCo === undefined ? borderColor : _props$borderBottomCo,
      _props$borderLeftColo = props.borderLeftColor,
      borderLeftColor = _props$borderLeftColo === undefined ? borderColor : _props$borderLeftColo,
      _props$borderRightCol = props.borderRightColor,
      borderRightColor = _props$borderRightCol === undefined ? borderColor : _props$borderRightCol,
      _props$borderTopColor = props.borderTopColor,
      borderTopColor = _props$borderTopColor === undefined ? borderColor : _props$borderTopColor,
      _props$fill = props.fill,
      fill = _props$fill === undefined ? false : _props$fill,
      _props$block = props.block,
      block = _props$block === undefined ? false : _props$block,
      _props$center = props.center,
      center = _props$center === undefined ? false : _props$center,
      restProps = _objectWithoutProperties(props, ['as', 'isReactNative', 'style', 'margin', 'marginHorizontal', 'marginVertical', 'marginBottom', 'marginLeft', 'marginRight', 'marginTop', 'padding', 'paddingHorizontal', 'paddingVertical', 'paddingBottom', 'paddingLeft', 'paddingRight', 'paddingTop', 'bottom', 'height', 'left', 'maxHeight', 'maxWidth', 'minHeight', 'minWidth', 'right', 'top', 'width', 'alignItems', 'alignSelf', 'flex', 'flexBasis', 'flexDirection', 'flexGrow', 'flexShrink', 'flexWrap', 'justifyContent', 'backgroundColor', 'opacity', 'overflow', 'position', 'zIndex', 'border', 'borderStyle', 'borderWidth', 'borderBottomWidth', 'borderLeftWidth', 'borderRightWidth', 'borderTopWidth', 'borderRadius', 'borderBottomLeftRadius', 'borderBottomRightRadius', 'borderTopLeftRadius', 'borderTopRightRadius', 'borderColor', 'borderBottomColor', 'borderLeftColor', 'borderRightColor', 'borderTopColor', 'fill', 'block', 'center']);

  var boxStyle = _extends({}, isReactNative ? null : reactNativeEmulationForBrowsers, fill ? { flex: 1 } : null, block ? { flexDirection: 'column' } : null, center ? { justifyContent: 'center' } : null, {

    marginBottom: marginBottom,
    marginLeft: marginLeft,
    marginRight: marginRight,
    marginTop: marginTop,

    paddingBottom: paddingBottom,
    paddingLeft: paddingLeft,
    paddingRight: paddingRight,
    paddingTop: paddingTop,

    bottom: bottom,
    height: height,
    left: left,
    maxHeight: maxHeight,
    maxWidth: maxWidth,
    minHeight: minHeight,
    minWidth: minWidth,
    right: right,
    top: top,
    width: width
  }, justValue({
    alignItems: alignItems,
    alignSelf: alignSelf,
    flexBasis: flexBasis,
    flexDirection: flexDirection,
    flexGrow: flexGrow,
    flexShrink: flexShrink,
    flexWrap: flexWrap,
    justifyContent: justifyContent,
    opacity: opacity,
    overflow: overflow,
    position: position,
    zIndex: zIndex,
    border: border,
    borderStyle: borderStyle,
    borderBottomWidth: borderBottomWidth,
    borderLeftWidth: borderLeftWidth,
    borderRightWidth: borderRightWidth,
    borderTopWidth: borderTopWidth,
    borderBottomLeftRadius: borderBottomLeftRadius,
    borderBottomRightRadius: borderBottomRightRadius,
    borderTopLeftRadius: borderTopLeftRadius,
    borderTopRightRadius: borderTopRightRadius
  }), restrictedFlex(flex, flexBasis, flexShrink, isReactNative), {
    // ...themeColor(theme.colors, {
    //   backgroundColor,
    //   borderBottomColor,
    //   borderLeftColor,
    //   borderRightColor,
    //   borderTopColor
    // }),
    backgroundColor: backgroundColor,
    borderBottomColor: borderBottomColor,
    borderLeftColor: borderLeftColor,
    borderRightColor: borderRightColor,
    borderTopColor: borderTopColor
  }, style);

  var fref = restProps.fref,
      rawStyle = restProps.rawStyle,
      className = restProps.className,
      restPropsWithoutRef = _objectWithoutProperties(restProps, ['fref', 'rawStyle', 'className']);

  // strip undefined


  var keys = R.filter(function (key) {
    return boxStyle[key] !== undefined;
  }, R.keys(boxStyle));

  var classNameFinal = renderer.renderRule(function () {
    return R.zipObj(keys, R.map(function (key) {
      return boxStyle[key];
    }, keys));
  });

  return _react2.default.createElement(as || 'div', _extends({}, restPropsWithoutRef, {
    ref: fref,
    style: rawStyle,
    className: classNameFinal + (className ? ' ' + className : '')
  }));
};

Box.contextTypes = {
  renderer: _propTypes2.default.object
};

//withTheme(Box);

exports.default = Box;