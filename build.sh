#!/usr/bin/env bash

DIR=$(pwd)

_npm() {
docker run \
	--volume $DIR:/opt \
	--workdir /opt \
	--user $UID:$GID \
	node:latest $*
}

_npm npm install

rm -rf $DIR/dist/*
_npm npx babel src/ --out-dir dist/


git add dist/component/*
git commit . -m 'Build'
git push


