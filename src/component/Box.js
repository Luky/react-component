// @flow

import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';

//import {withTheme} from './fela.js';

//export type BoxProps = {
//  as?: string | ((props: Object) => React.Element<*>),
//  isReactNative?: boolean,
//  style?: Object,
//
//  margin?: MaybeRhythm,
//  marginHorizontal?: MaybeRhythm,
//  marginVertical?: MaybeRhythm,
//  marginBottom?: MaybeRhythm,
//  marginLeft?: MaybeRhythm,
//  marginRight?: MaybeRhythm,
//  marginTop?: MaybeRhythm,
//
//  padding?: MaybeRhythm,
//  paddingHorizontal?: MaybeRhythm,
//  paddingVertical?: MaybeRhythm,
//  paddingBottom?: MaybeRhythm,
//  paddingLeft?: MaybeRhythm,
//  paddingRight?: MaybeRhythm,
//  paddingTop?: MaybeRhythm,
//
//  bottom?: MaybeRhythm,
//  height?: MaybeRhythm,
//  left?: MaybeRhythm,
//  maxHeight?: MaybeRhythm,
//  maxWidth?: MaybeRhythm,
//  minHeight?: MaybeRhythm,
//  minWidth?: MaybeRhythm,
//  right?: MaybeRhythm,
//  top?: MaybeRhythm,
//  width?: MaybeRhythm,
//
//  // Flexbox. Only what's compatible with React Native.
//  // github.com/facebook/react-native/blob/master/Libraries/StyleSheet/LayoutPropTypes.js
//  alignItems?: 'flex-start' | 'flex-end' | 'center' | 'stretch' | 'baseline',
//  alignSelf?: | 'auto'
//      | 'flex-start'
//      | 'flex-end'
//      | 'center'
//      | 'stretch'
//      | 'baseline',
//  flex?: number,
//  flexBasis?: number | string,
//  flexDirection?: 'row' | 'row-reverse' | 'column' | 'column-reverse',
//  flexGrow?: number,
//  flexShrink?: number,
//  flexWrap?: 'wrap' | 'nowrap',
//  justifyContent?: | 'flex-start'
//      | 'flex-end'
//      | 'center'
//      | 'space-between'
//      | 'space-around',
//
//  backgroundColor?: Color,
//  opacity?: number,
//  overflow?: 'visible' | 'hidden' | 'scroll',
//  position?: 'absolute' | 'relative',
//  zIndex?: number,
//
//  border?: string,
//  borderStyle?: 'solid' | 'dotted' | 'dashed',
//  borderWidth?: number,
//  borderBottomWidth?: number,
//  borderLeftWidth?: number,
//  borderRightWidth?: number,
//  borderTopWidth?: number,
//
//  borderRadius?: number,
//  borderBottomLeftRadius?: number,
//  borderBottomRightRadius?: number,
//  borderTopLeftRadius?: number,
//  borderTopRightRadius?: number,
//
//  borderColor?: Color,
//  borderBottomColor?: Color,
//  borderLeftColor?: Color,
//  borderRightColor?: Color,
//  borderTopColor?: Color
//};

//type BoxContext = ThemeContext & { renderer: { renderRule: () => string } };

// Emulate React Native to ensure the same styles for all platforms.
// https://facebook.github.io/yoga
// https://github.com/Microsoft/reactxp
// https://github.com/necolas/react-native-web
const reactNativeEmulationForBrowsers = {
  display: 'flex',
  // flexDirection: 'column',
  position: 'relative',
};

const reduce = (props, getValue) =>
    Object.keys(props).reduce((style, prop) => {
      const value = props[prop];
      if (value === undefined) return style;
      return {
        ...style,
        [prop]: getValue(value),
      };
    }, {});

const justValue = props => reduce(props, value => value);

// https://facebook.github.io/react-native/releases/0.44/docs/layout-props.html#flex
// https://github.com/necolas/react-native-web expandStyle-test.js
const restrictedFlex = (
    flex,
    flexBasis  = 'auto',
    flexShrink = 1,
    isReactNative) => {
  if (flex === undefined) return null;
  if (flex < 1) throw new Error('Not implemented yet');
  return isReactNative ? {flex} : {flexBasis, flexGrow: flex, flexShrink};
};

// Color any type, because Flow can't infere props for some reason.
//const themeColor = (colors: any, props) =>
//    reduce(props, value => colors[value]);

const Box = (props, {renderer}) => {
  const {
          as,
          isReactNative,
          style,

          margin,
          marginHorizontal        = margin,
          marginVertical          = margin,
          marginBottom            = marginVertical,
          marginLeft              = marginHorizontal,
          marginRight             = marginHorizontal,
          marginTop               = marginVertical,

          padding,
          paddingHorizontal       = padding,
          paddingVertical         = padding,
          paddingBottom           = paddingVertical,
          paddingLeft             = paddingHorizontal,
          paddingRight            = paddingHorizontal,
          paddingTop              = paddingVertical,

          bottom,
          height,
          left,
          maxHeight,
          maxWidth,
          minHeight,
          minWidth,
          right,
          top,
          width,

          alignItems,
          alignSelf,
          flex,
          flexBasis,
          flexDirection,
          flexGrow,
          flexShrink,
          flexWrap,
          justifyContent,
          backgroundColor,
          opacity,
          overflow,
          position,
          zIndex,

          border,
          borderStyle,
          borderWidth,
          borderBottomWidth       = borderWidth,
          borderLeftWidth         = borderWidth,
          borderRightWidth        = borderWidth,
          borderTopWidth          = borderWidth,

          borderRadius,
          borderBottomLeftRadius  = borderRadius,
          borderBottomRightRadius = borderRadius,
          borderTopLeftRadius     = borderRadius,
          borderTopRightRadius    = borderRadius,

          borderColor,
          borderBottomColor       = borderColor,
          borderLeftColor         = borderColor,
          borderRightColor        = borderColor,
          borderTopColor          = borderColor,

          // My Shortcut's
          fill                    = false,
          block                   = false,
          center                  = false,

          ...restProps
        } = props;

  const boxStyle = {
    ...(isReactNative ? null : reactNativeEmulationForBrowsers),

    ...(fill ? {flex: 1} : null),
    ...(block ? {flexDirection: 'column'} : null),

    ...(center ? {justifyContent: 'center'} : null),

    marginBottom,
    marginLeft,
    marginRight,
    marginTop,

    paddingBottom,
    paddingLeft,
    paddingRight,
    paddingTop,

    bottom,
    height,
    left,
    maxHeight,
    maxWidth,
    minHeight,
    minWidth,
    right,
    top,
    width,
    ...justValue({
      alignItems,
      alignSelf,
      flexBasis,
      flexDirection,
      flexGrow,
      flexShrink,
      flexWrap,
      justifyContent,
      opacity,
      overflow,
      position,
      zIndex,
      border,
      borderStyle,
      borderBottomWidth,
      borderLeftWidth,
      borderRightWidth,
      borderTopWidth,
      borderBottomLeftRadius,
      borderBottomRightRadius,
      borderTopLeftRadius,
      borderTopRightRadius,
    }),
    ...restrictedFlex(flex, flexBasis, flexShrink, isReactNative),
    // ...themeColor(theme.colors, {
    //   backgroundColor,
    //   borderBottomColor,
    //   borderLeftColor,
    //   borderRightColor,
    //   borderTopColor
    // }),
    backgroundColor,
    borderBottomColor,
    borderLeftColor,
    borderRightColor,
    borderTopColor,
    ...style,
  };

  const {fref, rawStyle, className, ...restPropsWithoutRef} = restProps;

  // strip undefined
  const keys = R.filter(key => boxStyle[key] !== undefined, R.keys(boxStyle));

  const classNameFinal = renderer.renderRule(() =>
      R.zipObj(keys, R.map(key => boxStyle[key], keys)),
  );

  return React.createElement(as || 'div', {
    ...restPropsWithoutRef,
    ref: fref,
    style: rawStyle,
    className: classNameFinal + (className ? ` ${className}` : ''),
  });
};

Box.contextTypes = {
  renderer: PropTypes.object,
};

//withTheme(Box);

export default Box;
