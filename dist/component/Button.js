'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Text = require('./Text');

var _Text2 = _interopRequireDefault(_Text);

var _colors = require('./../colors');

var _colors2 = _interopRequireDefault(_colors);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var buttonBorderSize = '1px';
var buttonFontSize = '0.95em';
var buttonPadding = '4px 12px';
var buttonFontWeight = '400';
var buttonFontFamily = 'Muli';
//const buttonColorModifier = -18;

var base = function base() {
  return {
    margin: '5px',
    display: 'inline-block',
    fontSize: buttonFontSize,
    fontWeight: buttonFontWeight,
    padding: buttonPadding,
    textShadow: 'none',
    transition: 'all 0.4s ease',
    cursor: 'pointer'
  };
};

var colorized = function colorized(props) {

  var colorText = function (attr) {
    if (attr.color === 'facebook') return _colors2.default.colorFacebookText;
    if (attr.color === 'google') return _colors2.default.colorGooglePlusText;
    return _colors2.default.mGrey0;
  }(props);

  var colorBackground = function (attr) {
    if (attr.color === 'red') return _colors2.default.mRed4;
    if (attr.color === 'orange') return _colors2.default.mOrange4;
    if (attr.color === 'amber') return _colors2.default.mAmber4;
    if (attr.color === 'yellow') return _colors2.default.mYellow4;
    if (attr.color === 'green') return _colors2.default.mGreen4;
    if (attr.color === 'lime') return _colors2.default.mLime4;
    if (attr.color === 'cyan') return _colors2.default.mCyan4;
    if (attr.color === 'lightblue') return _colors2.default.mLightBlue4;
    if (attr.color === 'blue') return _colors2.default.mBlue4;
    if (attr.color === 'teal') return _colors2.default.mTeal4;
    if (attr.color === 'purple') return _colors2.default.mPurple4;
    if (attr.color === 'brown') return _colors2.default.mBrown4;
    if (attr.color === 'grey') return _colors2.default.mBlueGrey4;
    if (attr.color === 'black') return _colors2.default.mGrey8;
    if (attr.color === 'facebook') return _colors2.default.colorFacebook;
    if (attr.color === 'google') return _colors2.default.colorGooglePlus;
    if (attr.color === 'female') return _colors2.default.mPink5;
    if (attr.color === 'male') return _colors2.default.mIndigo6;
    if (attr.color === 'Cosplay') return _colors2.default.mDeepPurple7;
    if (attr.color === 'Photo') return _colors2.default.mAmber7;

    return _colors2.default.mIndigo4;
  }(props);

  var colorBackgroundHover = function (attr) {
    if (attr.color === 'facebook') return _colors2.default.colorFacebookHover;
    if (attr.color === 'google') return _colors2.default.colorGooglePlusHover;

    return _colors2.default.mIndigo4;
  }(props);

  var invert = {
    background: colorText,
    color: colorBackground,
    borderColor: colorBackground
  };

  return _extends({
    fontFamily: buttonFontFamily,
    color: colorText,
    background: colorBackground,
    border: buttonBorderSize + ' transparent solid',

    '&:hover, &:active, &:focus': {
      color: colorBackgroundHover - 15,
      borderColor: colorBackgroundHover - 15,
      background: colorBackgroundHover
    }
  }, props.invert ? invert : {});
};

var Button = function Button(props) {

  return _react2.default.createElement(_Text2.default, _extends({
    alignSelf: 'flex-start',
    style: _extends({}, base(props), colorized(props))
  }, props));
};

exports.default = Button;